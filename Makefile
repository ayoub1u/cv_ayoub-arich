CPPFLAGS= -Wall -pedantic 
CXX=gcc
BIN=list
all:list town
list:./obj/main.o ./obj/list.o
	$(CXX) $(CPPFLAGS)  $^ -o ./bin/$@ -lm

./obj/list.o : ./src/list.c ./include/list.h
	$(CXX) $(CPPFLAGS)  -I ./include/ $< -o $@ -c -lm 

town:./obj/testtown.o ./obj/town.o ./obj/list.o ./obj/road.o ./obj/graph.o
	$(CXX) $(CPPFLAGS)  $^ -o ./bin/$@ -lm

./obj/town.o : ./src/town.c ./include/list.h ./include/town.h
	$(CXX) $(CPPFLAGS)  -I ./include/ $< -o $@ -c -lm

./obj/testtown.o: ./src/testtown.c ./include/town.h ./include/road.h  ./include/graph.h
	$(CXX) $(CPPFLAGS) -I  ./include/ $< -o $@ -c -lm

./obj/road.o : ./src/road.c ./include/list.h ./include/town.h ./include/road.h
	$(CXX) $(CPPFLAGS)  -I ./include/ $< -o $@ -c -lm

./obj/graph.o : ./src/graph.c ./include/list.h ./include/town.h ./include/road.h ./include/graph.h
	$(CXX) $(CPPFLAGS)  -I ./include/ $< -o $@ -c -lm

./obj/main.o: ./src/main.c ./include/list.h
	$(CXX) $(CPPFLAGS) -I  ./include/ $< -o $@ -c -lm

clean:
	rm -f ./obj/*.o
	rm $(BIN)
