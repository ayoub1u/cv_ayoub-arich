#include "town.h"
#include "road.h"
#include "list.h"

struct town * createTown(char * name) {
  struct town *T = malloc(sizeof (struct town));
  T->name =(char*) malloc(sizeof(char));
  T->alist=(struct list*)malloc(sizeof(struct list));
  if (T != NULL){
    strcpy(T->name,name);
    T->alist = new();
  }
  return T;
}

void freeTown ( struct town * T ) {
  free(T->name);
  //T->alist = NULL;
  dellist(T->alist,NULL);
  free(T);
}

char * getTownName(struct town * T) {
  if(T){
    return T->name;  
  }
  return -1;
}

struct list * getAList(struct town * T) {
  if(T){
    return T->alist;
  }
  return -1;
}

void viewTown (struct town * T) {
  if(T){
    struct list* listRoad = getAList(T);
    printf("The town : %s\n",getTownName(T));
    viewlist(listRoad,*viewRoad);
  }
}
  

void printTownName ( struct town * T ) {
  if(T){
    printf("Town : %s", getTownName(T));
  }
}
