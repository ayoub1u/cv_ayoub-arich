#include "road.h"
#include "town.h"
#include "graph.h"
void testinsert_ordered(){
  char c[] ="LA";
  struct town *tLA =createTown(c);
  char c1[] ="NY";
  struct town *tNY =createTown(c1);
  char c2[] ="SF";
  struct town *tSF =createTown(c2);
  struct list* l=new();
  struct road *roadLAtoNY = createRoad(tLA,tNY);
  struct road *roadLAtoSF = createRoad(tLA,tSF);
  cons(tLA->alist,roadLAtoNY);
  cons(tLA->alist,roadLAtoSF);
  cons(l,tLA);
  printf("%d \n",l->numelm);
  insert_ordered(l,tNY,NULL,*getTownName);
  insert_ordered(l,tSF,NULL,*getTownName);
  viewlist(l,*viewTown);
  printf("%d \n",l->numelm);
}


void testGraph(){
  char c[] ="LA";
  struct town *tLA =createTown(c);
  char c1[] ="NY";
  struct town *tNY =createTown(c1);
  char c2[] ="SF";
  struct town *tSF =createTown(c2);
  struct list* l=new();
  struct road *roadLAtoNY = createRoad(tLA,tLA);
  struct road *roadLAtoSF = createRoad(tLA,tSF);
  struct road *roadNYtoLA = createRoad(tNY,tLA);
  struct road *roadSFtoLA = createRoad(tSF,tLA);
  graph G = new();
  cons(G,tSF);
  cons(G,tNY);
  cons(G,tLA);
  cons(tLA->alist,roadLAtoNY);
  cons(tLA->alist,roadLAtoSF);
  cons(tNY->alist,roadNYtoLA);
  cons(tSF->alist,roadSFtoLA);
  viewmap(G);
  
  freeGraph(G);
  viewmap(G);
}


int main(){
  printf("####################################################");
  testinsert_ordered();
  printf("####################################################");
  testGraph();
}
