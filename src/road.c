#include <stdlib.h>
#include "road.h"
#include "town.h"

struct road * createRoad (struct town * U, struct town * V) {
  if(U && V){
    struct road* R = malloc(sizeof(struct road));
    R->U = U;
    R->V = V;
    return R;
  }else{
    return -1;
  }
}

void freeRoad ( struct road * R ) {
  freeTown(getURoad(R));
  freeTown(getVRoad(R));
  free(R);
}

struct town * getURoad(struct road * R) {
  if(R){
    return R->U;
  }else{
    return -1;
  }
}

void setURoad ( struct road * R, struct town * T ) {

  if(!T){
    R->U = NULL;
  }else{
    R->U = createTown(getTownName(T));
    R->U->alist = T->alist;
  }
}

struct town * getVRoad(struct road * R) {
  if(R){
    return R->V;
  }else{
    return -1;
  }

}

void setVRoad ( struct road * R, struct town * T ) {
  if(!T){
    R->V = NULL;
  }else{
    R->V = createTown(getTownName(T));
    R->V->alist = T->alist;
  }
}

void viewRoad ( struct road * R ) {
  printTownName(getURoad(R));
  printf("   to   ");
  printTownName(getVRoad(R));
  printf("\n");
}
