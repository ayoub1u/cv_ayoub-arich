#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "town.h"
#include "road.h"
#include<string.h>
struct list * new () {
  /* TODO */
  struct list *L = malloc(sizeof (list));
  if (L != NULL)
    {
        L->numelm = 0;
        L->head = NULL;
        L->tail = NULL;
    }
    return L;
}

bool isempty ( struct list * L ) {
  /* TODO */
  if(L->numelm == 0){
    return true;
  }else{
    return false;
  }
}

void dellist ( struct list * L, void (*ptrF) () ) {
  struct elmlist * iterator = L->head;

  if ( ptrF == NULL ) { // ne supprime pas les données
    /* TODO */
    while(L->numelm>0){
      L->head=L->head->suc;
      if(L->numelm==1){
	L->tail = NULL;}
      free(iterator);
      L->numelm--;
    }
    
  } else { // suppression complète
    while(L->numelm>0){
      L->head=L->head->suc;
      if(L->numelm==1){
        L->tail = NULL;
      }
      free(iterator);
      (*ptrF)(iterator->data);
      L->numelm--;
    }

  }
  /* TODO */
  free(iterator);
}


void viewlist ( struct list * L, void (*ptrF) () ) {
  /* TODO */
  struct elmlist *E = L->head;
   while(E)
   {
     (*ptrF)(E->data);
     E = E->suc;
   }
   free(E);
}

void cons ( struct list * L, void * data ) {
  struct elmlist * E = (struct elmlist *) calloc( 1, sizeof(struct elmlist));
  E->data = malloc(sizeof(data));
  E->data = data;
  E->suc = L->head;
  E->pred = NULL;
  if(isempty(L)){
    L->tail = E;
  }else{
    L->head->pred = E;
    
  }
  L->head = E;
  L->numelm++;
}


  /* TODO */


void insert_after(struct list * L, void * data, struct elmlist * ptrelm) {
  struct elmlist * E = (struct elmlist *) calloc( 1, sizeof(struct elmlist));
  struct elmlist * T = (struct elmlist *) calloc( 1, sizeof(struct elmlist));
  E = L->head;
  T->data = malloc(sizeof(data));
  T->data = data;
  if(isempty(L)){   
    exit(EXIT_FAILURE);
  }
  L->numelm++;
  while(E != NULL){
    if(E == ptrelm){
      T->pred = ptrelm;
      T->suc = ptrelm->suc;
      ptrelm->suc = T;
      break;
    }else{
      E = E->suc;
    }
  }
}






  /* TODO */




void insert_position(struct list * L,int pos, void * data){
  //struct elmlist * E = (struct elmlist *) calloc( 1, sizeof(struct elmlist));
  int i;
  struct elmlist *newE, *tmp;
  newE->data = data;
  tmp = L->head;
  for (i = 1; i < pos; ++i)
    tmp = tmp->suc;
  newE->suc = tmp->suc;
  newE->pred = tmp;
  if(tmp->suc == NULL)
    L->tail = newE;
  else
    tmp->suc->pred = newE;
  tmp->suc = newE;
  L->numelm++;

}



void insert_ordered ( struct list * L, void * data, struct town * departure,char* (*ptrF) () ) {
  struct elmlist * E = (struct elmlist *) calloc( 1, sizeof(struct elmlist));
  struct elmlist * iterator = L->head;
  int i=0;
  E->data = malloc(sizeof(data));
  E->data = data;

  if( L->head == NULL) {
   /* TODO */
    cons(L,data);
 } else {
    if( departure == NULL) { // C'est la liste des villes
      /* TODO */
      
      struct elmlist * iterator = L->head;
       while(i != L->numelm+1 ){
	 if(strcmp((*ptrF)(iterator->data),(*ptrF)(data))>0){//pointeur sur la fct getNameTown
	   
	   insert_after(L,data,iterator->pred);
	   break;
	 }
	 if(iterator->suc==NULL){
	   
	   iterator->suc=E;
           E->suc=NULL;
           E->pred=iterator;
	   L->numelm++;
           break;
         }
	   i++;
	   iterator=iterator->suc;
       }
    } else { // C'est une liste d'ajacence, une liste de routes
      /* TODO */
      cons(L,data);
    }
 
    if ( iterator == NULL) { // Ajout en queue
      /* TODO */
    } else if ( iterator == L->head ) { // Ajout en Tête
      /* TODO */
    } else {
      /* TODO */
    }
  }
}
