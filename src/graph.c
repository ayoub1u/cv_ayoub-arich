#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "graph.h"
#include "road.h"
#include "town.h"

graph readmap() {
  
}
  
void viewmap ( graph G ) {
  viewlist(G,*viewTown);
}

void freeGraph ( graph G ) {
  /** TODO 
   * Il faut malgré tout supprimer les villes ET les routes.
   * 
   * Faites attention que les routes ne sont référencées que par
   * les listes d'adjacence des villes.
   * 
   * Vous devez parcourir les villes (le graphe G)
   *  Pour chaque ville V, parcourez sa liste d'adjance
   *    (liste des routes auxquelles V est connexe)
   *    Pour chaque route R de la liste d'adjacence,
   *      vous devez déréférencer (NUll) la ville V
   *      Si les deux villes U et V de R sont déréférencées
   *      Alors vous pouvez supprimer la route R
   */
  //dellist(G,*freeTown);
  struct elmlist * E = (struct elmlist *) calloc( 1, sizeof(struct elmlist)) ;
  struct elmlist * iteratorRoad = (struct elmlist *) calloc( 1, sizeof(struct elmlist)) ;
  E= G->head;
  struct town* v = E;
  struct town* N = NULL;
  struct road * r;
  while(E!=NULL){
    v = E->data;
    iteratorRoad = v->alist->head;
    while(iteratorRoad!=NULL){
      r = iteratorRoad->data;
      setURoad(r,N);
      if(!r->V){
    
	freeRoad(r);

      }
      iteratorRoad = iteratorRoad->suc;
    }
    //freeTown(v);
    E = E->suc;
  }
}
